var isMobile = window.matchMedia('(max-width: 767px)').matches;
var timeTag = '<input class="time" type="text"/>';
var timeRange = `${timeTag} – ${timeTag}`;

var setDates = function() {
    $('.date').datepicker();
    $('.date').datepicker('option', 'dateFormat', 'dd/mm/yy');
}

var totalCol = function(col) {
    var selector = $(`.${col}`);
    var totalSelector = $(`.${col}Total`)

    selector.on('input', function() {
        var total = 0.0;
        selector.each(function() {
            total += parseFloat($(this).val(), 10) || 0;
        });
        totalSelector.text(total);
    });
}

var insertRow = function(result) {
    $(result)
        .insertBefore('.total')
        .ready(function () {
            setDates();

            if (isMobile) {
                $('.row').on('press', function() {
                    navigator.vibrate(500);
                    $(this).remove();
                    return false;
                });
            } else {
                $('.row').on('contextmenu', function() {
                    $(this).remove();
                    return false;
                });
            }

            $('.shiftCell').click(function() {
                $(this).children(':first').toggle();
            })
            // .mouseout(function() {
            //     $(this).children(':first').hide();
            // });

            $('.single').click(function() {
                $(this).parent().replaceWith(`<p>${timeRange}</p>`);
                $('.time').clockTimePicker();
            });

            $('.double').click(function() {
                var gparent = $(this).parent().parent();
                $(this).parent().replaceWith(`<p>${timeRange}<br>${timeRange}</p>`);
                $('.time').clockTimePicker();
                gparent.next().addClass('doubleCell');
            });

            $('.triple').click(function() {
                var gparent = $(this).parent().parent();
                $(this).parent().replaceWith(`<p>${timeRange}<br>${timeRange}<br>${timeRange}</p>`);
                $('.time').clockTimePicker();
                gparent.next().addClass('tripleCell');
            });

            totalCol('reg');
            totalCol('ot');
        });
    return false;
}

$(function() {
    setDates();

    $.get('/aas-form-gen/row', function(result) {
        var insert = function() {
            insertRow(result)
        };

        for (var i = 0; i < 11; i++) {
            insert();
        }

        $('.hours > .dTBody > .dTRow:first-child').click(insert);
    });

    $('.signature .dTCell:last-child').signature();
    $('canvas').prop('height', 90);
});